from app import app
import os


@app.route("/", methods=["GET"])
@app.route("/index", methods=["GET"])
def index():
    return "Welcome User! You are authenticated to use the API."


@app.route("/database", methods=["GET"])
def database():
    return {"CLUSTER_URL": f"mongodb://{os.environ.get('DB_USERNAME')}:{os.environ.get('DB_PASSWORD')}@{os.environ.get('CLUSTER_ENDPOINT')}:27017/",
            "USERNAME": os.environ.get('DB_USERNAME'),
            "PASSWORD": os.environ.get('DB_PASSWORD', None),
            "DATA": {"age": 50, "height": 182, "weight": 85}}